#include "KillEmAllGameMode.h"
#include "EngineUtils.h"
#include "GameFramework/Controller.h"
#include "TPSAIController.h"

void AKillEmAllGameMode::PawnKilled(APawn* PawnKilled)
{
    Super::PawnKilled(PawnKilled);

    APlayerController* PlayerController = Cast<APlayerController>(PawnKilled->GetController());
    if(PlayerController != nullptr)
    {
        GameEnded(false);
    }

    for (ATPSAIController* AIController : TActorRange<ATPSAIController>(GetWorld()))    
    {
        if(AIController != nullptr) 
        {
            if(!AIController->IsDead())
            {
                return;
            }
        }
    }

    GameEnded(true);
    
}

void AKillEmAllGameMode::GameEnded(bool bIsPlayerWinner)
{
    for (AController* Controller : TActorRange<AController>(GetWorld()))
    {
        if(Controller != nullptr)
        {
            bool bIsWinner = Controller->IsPlayerController() == bIsPlayerWinner; 
            Controller->GameHasEnded(Controller->GetPawn(), bIsWinner);
        }
    }    
}
