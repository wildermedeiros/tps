#include "BTService_PlayerLocationIfSeen.h"
#include "Kismet/GameplayStatics.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "TPSCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"

UBTService_PlayerLocationIfSeen::UBTService_PlayerLocationIfSeen()
{
    NodeName = "Update Player Location If Seen";
}

void UBTService_PlayerLocationIfSeen::TickNode(UBehaviorTreeComponent &OwnerComp, uint8 *NodeMemory, float DeltaSeconds)
{
    APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
    if(PlayerPawn == nullptr)
    {
        return;
    }
    
    AAIController* AIController = OwnerComp.GetAIOwner();
    if(AIController == nullptr)
    {
        return;
    }

    if (AIController->LineOfSightTo(PlayerPawn))
    {
        OwnerComp.GetBlackboardComponent()->SetValueAsObject(GetSelectedBlackboardKey(), PlayerPawn);
        SetWalkSpeed(OwnerComp, 600);

    }
    else
    {
        OwnerComp.GetBlackboardComponent()->ClearValue(GetSelectedBlackboardKey());
        SetWalkSpeed(OwnerComp, 300);
    }
}

void UBTService_PlayerLocationIfSeen::SetWalkSpeed(UBehaviorTreeComponent &OwnerComp, float NewWalkSpeed)
{
    AAIController *AIController = OwnerComp.GetAIOwner();
    if (AIController == nullptr)
    {
        return;
    }

    ATPSCharacter *AICharacter = Cast<ATPSCharacter>(AIController->GetCharacter());
    if (AICharacter == nullptr)
    {
        return;
    }

    UCharacterMovementComponent *CharacterMoveComponent = AICharacter->FindComponentByClass<UCharacterMovementComponent>();
    if (CharacterMoveComponent == nullptr)
    {
        return;
    }
    CharacterMoveComponent->MaxWalkSpeed = NewWalkSpeed;
}


