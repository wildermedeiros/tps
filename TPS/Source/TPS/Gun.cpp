#include "Gun.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/DamageEvents.h"
#include "TPSCharacter.h"

AGun::AGun()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;
	
	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
}

void AGun::PullTrigger()
{
	UGameplayStatics::SpawnEmitterAttached(MuzzleParticle, Mesh, TEXT("MuzzleFlashSocket"));
	UGameplayStatics::SpawnSoundAttached(MuzzleSound, Mesh, TEXT("MuzzleFlashSocket"));

	FHitResult OutHit;
	FVector ShootDirection;
	bool bHasHit = GunTrace(OutHit, ShootDirection);
	if (bHasHit)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitImpactParticle, OutHit.ImpactPoint, ShootDirection.Rotation());
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitImpactSound, OutHit.Location);

		AActor* HitActor = OutHit.GetActor();
		if(HitActor == nullptr)
		{		
			return;
		}
		AController* OwnerController = GetOwnerController();
		if (OwnerController == nullptr) 
		{ 
			return;
		}
		FPointDamageEvent DamageEvent(Damage, OutHit, ShootDirection, nullptr);
		//ATPSCharacter* HitCharacter = Cast<ATPSCharacter>(HitActor);
		//HitCharacter->GetHealthPercentage
		HitActor->TakeDamage(Damage, DamageEvent, OwnerController, this);
	}

}

void AGun::BeginPlay()
{
	Super::BeginPlay();
}

void AGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

bool AGun::GunTrace(FHitResult& OutHit, FVector& ShootDirection) const
{
	AController* OwnerController = GetOwnerController();
	if (OwnerController == nullptr) 
	{ 
		return false;
	}

	FVector Location;
	FRotator Rotation;
	OwnerController->GetPlayerViewPoint(Location, Rotation);
	
	ShootDirection = -Rotation.Vector();

	FVector End = Location + Rotation.Vector() * MaxRange;

	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(GetOwner());
	return GetWorld()->LineTraceSingleByChannel(OutHit, Location, End, ECollisionChannel::ECC_GameTraceChannel1, Params);
}

AController* AGun::GetOwnerController() const
{
	APawn* OwnerPawn = Cast<APawn>(GetOwner());
	if(OwnerPawn == nullptr) 
	{ 
		return nullptr; 
	}
	return OwnerPawn->GetController();
}
