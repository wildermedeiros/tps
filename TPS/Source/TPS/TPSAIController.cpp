#include "TPSAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "TPSCharacter.h"

void ATPSAIController::BeginPlay()
{
    Super::BeginPlay();
    
    if (AIBehavior != nullptr)
    {
        RunBehaviorTree(AIBehavior);
    }

    APawn *AIControllerPawn = GetPawn();
    if (AIControllerPawn != nullptr)
    {
        GetBlackboardComponent()->SetValueAsVector(TEXT("StartLocation"), AIControllerPawn->GetActorLocation());
    }
}
void ATPSAIController::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

bool ATPSAIController::IsDead() const
{
    ATPSCharacter* AIControllerCharacter = Cast<ATPSCharacter>(GetPawn());
    if(AIControllerCharacter != nullptr)
    {
        return AIControllerCharacter->IsDead();
    }

    return true;
}
