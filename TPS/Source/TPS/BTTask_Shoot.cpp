#include "BTTask_Shoot.h"
#include "AIController.h"
#include "TPSCharacter.h"

UBTTask_Shoot::UBTTask_Shoot()
{
    NodeName = TEXT("Shoot");
}

EBTNodeResult::Type UBTTask_Shoot::ExecuteTask(UBehaviorTreeComponent &OwnerComp, uint8 *NodeMemory)
{
    Super::ExecuteTask(OwnerComp, NodeMemory);

    AAIController* AIController = OwnerComp.GetAIOwner();
    if (AIController == nullptr)
    {
        return EBTNodeResult::Failed;
    }
    ATPSCharacter* AIThirdPersonShooterCharacter = Cast<ATPSCharacter>(AIController->GetPawn());
    if (AIThirdPersonShooterCharacter != nullptr)
    {
        AIThirdPersonShooterCharacter->Shoot();
    }

    return EBTNodeResult::Succeeded;
}


