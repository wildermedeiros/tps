
#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "TPSAIController.generated.h"


UCLASS()
class TPS_API ATPSAIController : public AAIController
{
	GENERATED_BODY()

public:	
	virtual void Tick(float DeltaTime) override;

	bool IsDead() const;
	
protected:
	virtual void BeginPlay() override;

private:
	APawn *PlayerPawn;

	UPROPERTY(EditAnywhere)
	UBehaviorTree *AIBehavior;
};
